package com.example.cyh.app

import android.Manifest
import android.app.DownloadManager.Request.NETWORK_WIFI
import android.content.ContentProviderOperation
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.telephony.TelephonyManager
import android.view.View
import android.widget.Button
import android.widget.Toast
import android.support.v4.content.ContextCompat
import android.provider.Settings.Secure
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Looper
import android.provider.ContactsContract
import android.util.Log
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InputStream
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.NetworkInterface
import java.net.URL


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var flag: Boolean = false
    private var gsp_btn: Button? = null
    private var network_btn: Button? = null
    private var best_btn: Button? = null

    private var phone_type: Button? = null
    private var phone_type_no: Button? = null
    private var phone_imei: Button? = null
    private var phone_android_id: Button? = null
    private var phone_no: Button? = null
    private var phone_sim1: Button? = null
    private var ip: Button? = null
    private var phone_wifi: Button? = null
    private var phone_mac: Button? = null
    private var netwoking_mode: Button? = null
    private var outside_ip: Button? = null
    private var sendAllInfo: Button? = null
    private var mobile_carrier: Button? = null
    private var mobile_address_book: Button? = null
    private var bi = BaseInfo()
    private var insert_contacts: Button? = null
    private var class_loader:Button?=null
//    private var sim_operator_name:Button?=null
    private var delete_contacts:Button?=null
    private var icc:Button?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = applicationContext

        initView()
        initListener()

        basePhoneInfoInit()
        basePhoneInfoListenerInit()
    }

    private fun initListener() {
        gsp_btn!!.setOnClickListener(this)
        network_btn!!.setOnClickListener(this)
        best_btn!!.setOnClickListener(this)
        getString(R.string.best)
    }

    protected override fun onResume() {
        super.onResume()
        initPermission()//针对6.0以上版本做权限适配
    }

    private fun initView() {
        gsp_btn = findViewById(R.id.gps) as Button?
        network_btn = findViewById(R.id.net) as Button?
        best_btn = findViewById(R.id.best) as Button?
    }

    private fun basePhoneInfoInit() {
        phone_type = findViewById(R.id.phone_type) as Button?
        phone_type_no = findViewById(R.id.phone_type_no) as Button?
        phone_imei = findViewById(R.id.phone_imei) as Button?
        phone_android_id = findViewById(R.id.phone_android_id) as Button?
        phone_no = findViewById(R.id.phone_no) as Button?
        phone_sim1 = findViewById(R.id.phone_sim1) as Button?
        ip = findViewById(R.id.ip) as Button?
        phone_wifi = findViewById(R.id.phone_wifi) as Button?
        phone_mac = findViewById(R.id.phone_mac) as Button?
        netwoking_mode = findViewById(R.id.netwoking_mode) as Button?
        outside_ip = findViewById(R.id.outside_ip) as Button?
        sendAllInfo = findViewById(R.id.send_all_info) as Button?
        mobile_carrier = findViewById(R.id.mobile_carrier) as Button?
        mobile_address_book = findViewById(R.id.mobile_address_book) as Button?
        insert_contacts = findViewById(R.id.insert_contacts) as Button?
        class_loader = findViewById(R.id.class_loader) as Button?
        delete_contacts= findViewById(R.id.delete_contacts) as Button?
        icc= findViewById(R.id.icc) as Button?
    }

    private fun basePhoneInfoListenerInit() {
        phone_type!!.setOnClickListener(this)
        phone_type_no!!.setOnClickListener(this)
        phone_imei!!.setOnClickListener(this)
        phone_android_id!!.setOnClickListener(this)
        phone_no!!.setOnClickListener(this)
        phone_sim1!!.setOnClickListener(this)
        ip!!.setOnClickListener(this)
        phone_wifi!!.setOnClickListener(this)
        phone_mac!!.setOnClickListener(this)
        netwoking_mode!!.setOnClickListener(this)
        outside_ip!!.setOnClickListener(this)
        sendAllInfo!!.setOnClickListener(this)
        mobile_carrier!!.setOnClickListener(this)
        mobile_address_book!!.setOnClickListener(this)
        insert_contacts!!.setOnClickListener(this)
        class_loader!!.setOnClickListener(this)
        delete_contacts!!.setOnClickListener(this)
        icc!!.setOnClickListener(this)
    }

    private fun initPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //检查权限
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) !== PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) !== PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_PHONE_STATE
                ) !== PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_WIFI_STATE
                ) !== PackageManager.PERMISSION_GRANTED|| ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_NETWORK_STATE
                ) !== PackageManager.PERMISSION_GRANTED|| ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.INTERNET
                ) !== PackageManager.PERMISSION_GRANTED|| ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_CONTACTS
                ) !== PackageManager.PERMISSION_GRANTED|| ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_CONTACTS
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                //请求权限
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION
                        , Manifest.permission.ACCESS_FINE_LOCATION
                    ,Manifest.permission.READ_PHONE_STATE
                    ,Manifest.permission.ACCESS_WIFI_STATE
                    ,Manifest.permission.ACCESS_NETWORK_STATE
                    ,Manifest.permission.INTERNET
                    ,Manifest.permission.READ_CONTACTS
                    ,Manifest.permission.WRITE_CONTACTS
                    ),
                    1
                )
            } else {
                flag = true
            }
        } else {
            flag = true
        }
    }

    /**
     * 权限的结果回调函数
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            flag = grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] ==
                    PackageManager.PERMISSION_GRANTED
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.gps -> if (flag) {
                getGPSLocation()
            } else {
                Toast.makeText(this, "no permission", Toast.LENGTH_SHORT).show()
            }
            R.id.net -> if (flag) {
                getNetworkLocation()
            } else {
                Toast.makeText(this, "no permission", Toast.LENGTH_SHORT).show()
            }
            R.id.best -> if (flag) {
                getBestLocation()
            } else {
                Toast.makeText(this, "no permission", Toast.LENGTH_SHORT).show()
            }
            R.id.phone_type -> getPhoneType()
            R.id.phone_type_no -> getPhoneTypeNo()
            R.id.phone_imei -> getPhoneImei()
            R.id.phone_android_id -> getPhoneAndroidId()
            R.id.phone_no -> getPhoneNo()
            R.id.phone_sim1 -> getPhoneSim1()
            R.id.ip -> getPhoneIp()
            R.id.phone_wifi -> getPhoneWifi()
            R.id.phone_mac -> getPhoneMac()
            R.id.netwoking_mode -> getNetWorkingMode()
            R.id.outside_ip -> getOutsideIp()
            R.id.send_all_info -> sendAll()
            R.id.mobile_carrier -> getMobileCarrier()
            R.id.mobile_address_book -> getMobileAddressBook()
            R.id.insert_contacts -> insertContacts()
            R.id.class_loader-> getClassLoaderName()
            R.id.delete_contacts->deleteContacts()
            R.id.icc->getIcc()
        }
    }

    private fun getIcc() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            var telephonyManager: TelephonyManager =
                context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            Toast.makeText(this, "手机运营商名称 ${telephonyManager.simSerialNumber}", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "手机运营商名称 空", Toast.LENGTH_SHORT).show()
        }
    }

    private fun deleteContacts() {
        this.contentResolver.delete(Uri.parse("content://com.android.contacts/raw_contacts"),null,null)
        Toast.makeText(this, "删除完毕", Toast.LENGTH_SHORT).show()
    }

    //运营商名称
    private fun getSimOperatorName() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            var telephonyManager: TelephonyManager =
                context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            Toast.makeText(this, "手机运营商名称 ${telephonyManager.simOperatorName}", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "手机运营商名称 空", Toast.LENGTH_SHORT).show()
        }
    }

    //得到classloader
    private fun getClassLoaderName() {
       var c= classLoader
        Toast.makeText(this, "classLoader  ${c.javaClass.simpleName} networkinterface   ${NetworkInterface::class.java.classLoader.javaClass.simpleName}", Toast.LENGTH_SHORT).show()
    }

    //批量导入通信录
    private fun insertContacts() {
        Thread {
            var num =100
            var ops = ArrayList<ContentProviderOperation>()
            var rawContactInsertIndex: Int=0
            var start= System.currentTimeMillis()
            for (i in 0..num) {
                rawContactInsertIndex = ops.size
                ops.add(
                    ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                        .build()
                )

                //文档位置：reference\android\provider\ContactsContract.Data.html
                ops.add(
                    ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                        .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, i)
                        .build()
                )
                ops.add(
                    ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                        .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, "5556$rawContactInsertIndex")
                        .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                        .build()
                )

            }
            var results = this.contentResolver.applyBatch(ContactsContract.AUTHORITY, ops)
            var end= System.currentTimeMillis()
            Looper.prepare()
            Toast.makeText(this, "导入完毕用时 ${(end-start)/1000} s", Toast.LENGTH_SHORT).show()
            Looper.loop()
        }.start()
    }

    private fun getMobileAddressBook() {
        //获取手机通讯录联系人
        var cr = contentResolver
        var cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            //取得联系人名字
            val nameFieldColumnIndex = cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME)
            val contact = cursor.getString(nameFieldColumnIndex)
            Log.d("取得联系人名字", contact)
            //取得电话号码
            val contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
            var phone = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId,
                null,
                null
            )
            while (phone.moveToNext()) {
                val phoneNumber = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                Log.d("取得电话号码", phoneNumber)
            }
        }
    }
    //运营商编号imsi
    private fun getMobileCarrier() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            var telephonyManager: TelephonyManager =
                context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            Toast.makeText(this, "手机运营商  ${telephonyManager.subscriberId}", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "手机运营商 空", Toast.LENGTH_SHORT).show()
        }
    }

    //发送模拟信息
    private fun sendAll() {
        getGPSLocation()
        getNetworkLocation()
        getBestLocation()
        getPhoneType()
        getPhoneTypeNo()
        getPhoneImei()
        getPhoneAndroidId()
        getPhoneNo()
        getPhoneSim1()
        getPhoneIp()
        getPhoneWifi()
        getPhoneMac()
        getNetWorkingMode()
        Thread {
            var client = OkHttpClient()
            var formBody = FormBody.Builder().add("data", bi.toString())//设置参数名称和参数值
                .build();
            val request = Request.Builder().post(formBody).url("http://192.168.1.31:8075/edward/http/setInfo").build()
            var result = ""
            try {
                client.newCall(request).execute()//发送请求
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }.start()
    }


    /**
     * 通过GPS获取定位信息
     */
    fun getGPSLocation() {
        val gps = LocationUtils.getGPSLocation(this)
        if (gps == null) {
            //设置定位监听，因为GPS定位，第一次进来可能获取不到，通过设置监听，可以在有效的时间范围内获取定位信息
            LocationUtils.addLocationListener(
                context!!,
                LocationManager.GPS_PROVIDER,
                object : LocationUtils.ILocationListener {
                    override fun onSuccessLocation(location: Location) {
                        if (location != null) {
                            bi.latitude = location!!.latitude
                            bi.longitude = location!!.longitude
                            Toast.makeText(
                                this@MainActivity,
                                "gps onSuccessLocation location:  lat==" + location!!.latitude + "     lng==" + location!!.longitude,
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(this@MainActivity, "gps location is null", Toast.LENGTH_SHORT).show()
                        }
                    }
                })
        } else {
            Toast.makeText(this, "gps location: lat==" + gps.latitude + "  lng==" + gps.longitude, Toast.LENGTH_SHORT)
                .show()
        }
    }

    /**
     * 通过网络等获取定位信息
     */
    private fun getNetworkLocation() {
        val net = LocationUtils.getNetWorkLocation(this)
        if (net == null) {
            Toast.makeText(this, "net location is null", Toast.LENGTH_SHORT).show()
        } else {
            bi.latitude = net!!.latitude
            bi.longitude = net!!.longitude
            Toast.makeText(
                this,
                "network location: lat==" + net.latitude + "  lng==" + net.longitude, Toast.LENGTH_SHORT
            ).show()
        }
    }

    /**
     * 采用最好的方式获取定位信息
     */
    private fun getBestLocation() {
        val c = Criteria()//Criteria类是设置定位的标准信息（系统会根据你的要求，匹配最适合你的定位供应商），一个定位的辅助信息的类
        c.powerRequirement = Criteria.POWER_LOW//设置低耗电
        c.isAltitudeRequired = true//设置需要海拔
        c.bearingAccuracy = Criteria.ACCURACY_COARSE//设置COARSE精度标准
        c.accuracy = Criteria.ACCURACY_LOW//设置低精度
        //... Criteria 还有其他属性，就不一一介绍了
        val best = LocationUtils.getBestLocation(this, c)
        if (best == null) {
            Toast.makeText(this, " best location is null", Toast.LENGTH_SHORT).show()
        } else {
            bi.latitude = best!!.latitude
            bi.longitude = best!!.longitude
            Toast.makeText(this, "best location: lat==" + best.latitude + " lng==" + best.longitude, Toast.LENGTH_SHORT)
                .show()
        }
    }

    companion object {
        private var context: Context? = null
    }

    private fun getPhoneType() {
        bi.phoneType = android.os.Build.BRAND
        Toast.makeText(this, "手机品牌 ${android.os.Build.BRAND}", Toast.LENGTH_SHORT).show()
    }

    private fun getPhoneTypeNo() {
        bi.phoneTypeNo = android.os.Build.MODEL
        Toast.makeText(this, "手机型号 ${android.os.Build.MODEL}", Toast.LENGTH_SHORT).show()
    }

    private fun getPhoneImei() {
        val permissionCheck =
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_PHONE_STATE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                var telephonyManager: TelephonyManager =
                    context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                var imei = telephonyManager.deviceId
                bi.phoneImei = telephonyManager.deviceId
                Toast.makeText(this, "imei 号码 $imei", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "imei 号码  null", Toast.LENGTH_SHORT).show()
            }
    }

    private fun getPhoneAndroidId() {
        val s = Secure.getString(this.contentResolver, Secure.ANDROID_ID)
        bi.phone_android_id = s
        Toast.makeText(this, "AndroidId $s", Toast.LENGTH_SHORT).show()
    }

    private fun getPhoneNo() {
        bi.phone_no = android.os.Build.SERIAL
        Toast.makeText(this, "序列号  ${android.os.Build.SERIAL}", Toast.LENGTH_SHORT).show()
    }

    private fun getPhoneSim1() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            var telephonyManager: TelephonyManager =
                context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            bi.phone_sim1 = telephonyManager.line1Number
            Toast.makeText(this, "手机号  ${telephonyManager.line1Number}", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "手机号  null", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getPhoneIp() {
        var ip = WiFiUtil.getLocalIpAddress()
        bi.ip = ip
        Toast.makeText(this, "ip  $ip", Toast.LENGTH_SHORT).show()
    }


    private fun getPhoneWifi() {
        var wifiManager = context!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
        var wifiInfo = wifiManager.connectionInfo
        if (wifiInfo != null) {
            bi.phone_wifi = wifiInfo.ssid
            Toast.makeText(this, "mac 名称 ${wifiInfo.ssid}", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "mac 名称 null", Toast.LENGTH_SHORT).show()
        }

    }

    private fun getPhoneMac() {
        var mac = WiFiUtil.getLocalMacAddress(this)
        bi.phone_mac = mac
        Toast.makeText(this, "mac 地址 $mac", Toast.LENGTH_SHORT).show()
    }

    private fun getNetWorkingMode() {
        val connManager = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (null == connManager) { // 为空则认为无网络
            bi.netwoking_mode = "无网络"
            Toast.makeText(this, "无网络", Toast.LENGTH_SHORT).show()
            return
        }
        var activeNetInfo = connManager.activeNetworkInfo
        if (activeNetInfo == null || !activeNetInfo.isAvailable) {
            bi.netwoking_mode = "无网络"
            Toast.makeText(this, "无网络", Toast.LENGTH_SHORT).show()
            return
        }
        var wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (null != wifiInfo) {
            var state = wifiInfo.state;
            if (null != state) {
                if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                    bi.netwoking_mode = "Wifi"
                    Toast.makeText(this, "WIFI", Toast.LENGTH_SHORT).show()
                    return
                }
            }
        }
        var telephonyManager = context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        var networkType = telephonyManager.networkType
        when (networkType) {
            TelephonyManager.NETWORK_TYPE_GPRS,
            TelephonyManager.NETWORK_TYPE_CDMA,
            TelephonyManager.NETWORK_TYPE_EDGE,
            TelephonyManager.NETWORK_TYPE_1xRTT,
            TelephonyManager.NETWORK_TYPE_IDEN -> {
                bi.netwoking_mode = "2g"; Toast.makeText(this, "2G", Toast.LENGTH_SHORT).show()
            }
            TelephonyManager.NETWORK_TYPE_EVDO_A,
            TelephonyManager.NETWORK_TYPE_UMTS,
            TelephonyManager.NETWORK_TYPE_EVDO_0,
            TelephonyManager.NETWORK_TYPE_HSDPA,
            TelephonyManager.NETWORK_TYPE_HSUPA,
            TelephonyManager.NETWORK_TYPE_HSPA,
            TelephonyManager.NETWORK_TYPE_EVDO_B,
            TelephonyManager.NETWORK_TYPE_EHRPD,
            TelephonyManager.NETWORK_TYPE_HSPAP -> {
                bi.netwoking_mode = "3g";Toast.makeText(this, "3G", Toast.LENGTH_SHORT).show()
            }
            TelephonyManager.NETWORK_TYPE_LTE -> {
                bi.netwoking_mode = "4g";Toast.makeText(this, "4G", Toast.LENGTH_SHORT).show()
            }
            else -> {
                bi.netwoking_mode = "4g";Toast.makeText(this, "手机流量", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getOutsideIp() {
        Thread {
            var client = OkHttpClient()
            val request = Request.Builder().url("https://api.ip.sb/ip").build()
            var result = ""
            try {
                var response = client.newCall(request).execute()//发送请求
                result = response.body()!!.string()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            show(result)
        }.start()
    }

    private fun show(result: String) {
        Looper.prepare()
        Toast.makeText(this@MainActivity, " 外网ip $result", Toast.LENGTH_SHORT).show()
        Looper.loop()
    }

}
