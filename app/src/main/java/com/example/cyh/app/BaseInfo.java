package com.example.cyh.app;

import com.google.gson.Gson;

public class BaseInfo {
    public String phoneType="cyh fake"; //手机品牌
    public String phoneTypeNo="cyh fake";// 手机型号
    public String phoneImei="cyh fake";//IMEI
    public String phone_android_id="cyh fake";//Android_id
    public String phone_no="cyh fake";//序列号
    public String phone_sim1="cyh fake";//SIM卡号1
    public String ip="cyh fake";//ip
    public String phone_wifi="cyh fake";//Wifi链接名称
    public String phone_mac=" cyh fake";//mac地址
    public String netwoking_mode="cyh fake";//联网方式
    public Double longitude=1.1;//
    public Double latitude=1.1;//

    @Override
    public String toString() {
        return "BaseInfo{" +
                "phoneType='" + phoneType + '\'' +
                ", phoneTypeNo='" + phoneTypeNo + '\'' +
                ", phoneImei='" + phoneImei + '\'' +
                ", phone_android_id='" + phone_android_id + '\'' +
                ", phone_no='" + phone_no + '\'' +
                ", phone_sim1='" + phone_sim1 + '\'' +
                ", ip='" + ip + '\'' +
                ", phone_wifi='" + phone_wifi + '\'' +
                ", phone_mac='" + phone_mac + '\'' +
                ", netwoking_mode='" + netwoking_mode + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
